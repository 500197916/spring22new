<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Natalie Xiong Homepage</title>
    <link rel="stylesheet" type="text/css" href="css/base.css">

    <!--    app password: HJyY48LLBFwCcATN8PzU-->
    <!--    new-->
    
</head>
<body>
    <header>
        <h1>Natalie Xiong Homepage</h1>
    </header>

    <nav>
    <ul>

        <li><a href="#">Homepage</a></li>
        <li><a href="#">Loop demo</a></li>
        <li><a href="#">Countdown</a></li>
    </ul>

    </nav>

    <main>
        <img src="img/newhomepagePhoto.jpg" alt="Natalie Image">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci dignissimos dolorum incidunt magni
            maiores nemo nihil non optio, reiciendis tenetur. Amet architecto cum fuga itaque iure molestiae molestias
            numquam totam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aspernatur fugiat, molestias nulla odit perspiciatis voluptatum! At dolor doloremque exercitationem illo, illum minima pariatur praesentium quasi, reiciendis repellendus, vel vitae.</p>

    </main>

    <footer>

        &copy; <small> 2022 Natalie Xiong
    </footer>
</body>
</html>